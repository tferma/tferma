# *TerraFERMA* (repository deprecated)

**PLEASE VISIT [http://terraferma.github.io/](http://terraferma.github.io)**

From July 2015 this repository of TerraFERMA, the *Transparent Finite Element Rapid Model Assembler*, has been deprecated.  To find
out more about the ongoing TerraFERMA project please visit our new website at
[http://terraferma.github.io/](http://terraferma.github.io) or go directly to the new repository at
[https://github.com/TerraFERMA/TerraFERMA](https://github.com/TerraFERMA/TerraFERMA).

Many thanks for your interest in TerraFERMA!

